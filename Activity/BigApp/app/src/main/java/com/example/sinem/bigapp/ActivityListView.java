package com.example.sinem.bigapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityListView extends AppCompatActivity implements AdapterView.OnItemClickListener{
    ListView lst;
    String[] campers ={"Abby","Anna","Alex","Bryan","May","Shawna","Aaren","Abe","Nicky","Ashleigh","Shawn","Nick"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        lst = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,campers);
        lst.setAdapter(arrayAdapter);
        lst.setOnItemClickListener(this);
    }
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView tv = (TextView) view;
        Toast.makeText(this, "You click on" + tv.getText() + position, Toast.LENGTH_SHORT).show();
    }
}

