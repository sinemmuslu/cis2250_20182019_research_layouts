package com.example.sinem.bigapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityMain extends AppCompatActivity {


    public Button but1;
    public Button but2;
    public Button but3;
    public Button but4;
    public Button but5;
    public Button but6;
    public Button but7;
public void init(){
        but1= (Button)findViewById(R.id.butLinear);
        but1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent linear = new Intent(ActivityMain.this, ActivityLinear.class);
                startActivity(linear);

            }
        });

        but2= (Button)findViewById(R.id.butRelative);
        but2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent relative = new Intent(ActivityMain.this, ActivityRelative.class);
                startActivity(relative);

            }
        });

        but3= (Button)findViewById(R.id.butConstraint);
        but3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent constraint = new Intent(ActivityMain.this, ActivityConstraint.class);
                startActivity(constraint);

            }
        });

        but4= (Button)findViewById(R.id.butWeb);
        but4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent web = new Intent(ActivityMain.this, ActivityWeb.class);
                startActivity(web);

            }
        });
        but5= (Button)findViewById(R.id.butListView);
        but5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listview = new Intent(ActivityMain.this, ActivityListView.class);
                startActivity(listview);

            }
        });
        but6= (Button)findViewById(R.id.butFrame);
        but6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listview = new Intent(ActivityMain.this, ActivityFrame.class);
                startActivity(listview);

            }
        });
        but7= (Button)findViewById(R.id.butTable);
        but7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listview = new Intent(ActivityMain.this, ActivityTable.class);
                startActivity(listview);

            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();


    }
}
